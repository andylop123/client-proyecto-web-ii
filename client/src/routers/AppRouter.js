import React from 'react';
import{BrowserRouter as Router,Route,Switch} from 'react-router-dom'

import {Login,Register} from '../components/login/index'
import {Category} from '../components/admin/index'
import { Resources,News} from '../components/user/index';

export default function AppRouter(){
    return(
        <Router>
            <Switch>
                <Route exact path="/">
                    <Login/>
                </Route>

                <Route exact path="/Register">
                    <Register/>
                </Route>

                <Route exact path="/Category">   
                    <Category/>
                </Route>

                <Route exact path="/newsSources">  
                    <Resources/>
                </Route>
                <Route exact path="/news">  
                    <News/>
                </Route>


                <Route exact path="*">
                    <h1>404 Not found</h1>
                </Route>

            </Switch>

        </Router>
    )
}