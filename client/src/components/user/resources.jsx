import React from 'react';

//icons
import iconDelete from '../../assets/img/icons8_delete_bin_32px.png';
import iconEdit from '../../assets/img/icons8_edit_32px_1.png';
//modal
import { Modal, ModalBody, ModalFooter, ModalHeader, Input } from 'reactstrap';
//axios
import axios from 'axios';
//navBar component
import { NavBar } from './index';
//footer component
import { Footer } from './index';
//category url
//Redirect 
import { Redirect } from 'react-router-dom';
const url = 'http://localhost:3000/Category'

export class Resources extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            logOut: false,
            url: 'http://localhost:3000/newsSource',
            user: '',
            sources: [],
            modalInsertar: false,
            form: {
                id: '',
                name: '',
                url: '',
                category_id: '',
                tipoModal: ''
            },
            categories: []


        }
        this.logOut = this.logOut.bind(this);
        this.getUserLogin = this.getUserLogin.bind(this);

    }

  /*
    Este metodo selecciona las categorias guardadas en el servidor
    */
    getCategory = async () => {
        await axios.get(url).then(response => {

            this.setState({ categories: response.data })

        }).catch(err => {
            console.log(err.message)
        })


    }

     /*
    Este metodo selecciona las fuentes de noticias relaccionadas a un usuario
    */
    getSources = async () => {
        if (!this.state.logOut) {
            await axios.get(this.state.url, {
                params: {
                    user_id: this.state.user.user._id
                }
            }).then(response => {
                this.setState({ sources: response.data })

            }).catch(err => {
                console.log(err.message)
            })
        }
    }

     /*
    Este metodo inserta una nueva fuente de noticias en el servidor
    */
    postSources = async () => {
        await axios.post(this.state.url, this.state.form).then(response => {
            this.modalInsertar();
            this.getSources();
            this.cleanForm();
        }).catch(err => {
            console.log(err.message)
        })

    }

     /*
    Este metodo funciona para limpiar los campos de texto del form
    */
    cleanForm() {
        this.setState({
            form: {
                id: '',
                name: '',
                url: '',
                category_id: '',

            }
        })
    }

 /*
    Este metodo extrae los datos de una fuente de noticias del servidor
    */
    selectSource = (source) => {
        console.log(source)
        this.setState({
            tipoModal: 'actualizar',
            form: {
                id: source._id,
                name: source.name,
                url: source.url,
                user_id: source.user_id,
                category_id: source.category_id._id

            }
        });


    }

     /*
    Este metodo permite editar una fuente de noticias
    */
    patchSource = async () => {
        await axios.patch(this.state.url + '?id=' + this.state.form.id, this.state.form).then(response => {
            this.modalInsertar();
            this.getSources();
        }).catch(err => {
            console.log(err.message)
        })

    }
     /*
    Este metodo permite eliminar una fuente de noticias del servidor
    */

    deleteSource = async (id) => {
        console.log(id)
        await axios.delete(this.state.url + '?id=' + id).then(response => {
            this.getSources();
        }).catch(err => {
            console.log(err.message)
        })

    }



     /*
    Este metodo controla el modal el cual es contralado por una variable booleana
    */
    modalInsertar = () => {
        this.setState({ modalInsertar: !this.state.modalInsertar });
    }


     /*
    Este metodo es el encagado de llamar los metodo que traen las categorias y las  fuentes de noticias del servidor 
    */
    componentDidMount() {
        this.getCategory();
        this.getSources();
    }




 /*
    Este metodo extrae el usuario logueado
    */
    getUserLogin() {
        const userLogin = JSON.parse(localStorage.getItem('user'));
        if (userLogin == null) {
            this.logOut();
        } else {
            this.state.user = ({ user: userLogin });
        }

    }



    //this method make all the logic to logOut the user
    logOut() {
        localStorage.removeItem('user');
        this.state.logOut = ({ logOut: true })

    }



    /*
    Este metodo captura los datos de los campos de texto 
    */
    handleChange = async e => {
        e.preventDefault();
        let { form } = this.state;
        this.setState({
            form: {
                ...this.state.form,
                name: e.target.name == 'name' ? e.target.value : form.name,
                url: e.target.name == 'url' ? e.target.value : form.url,
                category_id: e.target.name == 'category_id' ? e.target.value : form.category_id,
                user_id: this.state.user.user._id

            }
        })

        console.log(this.state.form)
    }


    render() {
        const { form } = this.state;
        const { user } = this.state;

        if (user.length == 0) {
            this.getUserLogin();
        }

        if (this.state.logOut) {
            return <Redirect to='/' />
        }

        return (

            <div className="container">
                <NavBar />


                <div className="containter">
                    <div className="container mt-5 d-flex align-items-center flex-column bg-white text-secondary ">
                        <h4 className="display-6 text-center">NewsSources</h4>
                        <hr className="my-4 bg-secondary w-25" />
                    </div>

                </div>

                <div className="container ">
                    <main className="bg-white  d-flex flex-column align-items-center pt-4 pt-0 "  >
                        <table className="table  table-dark table-striped  w-50">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Url</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Actions </th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.state.sources.map(source => {
                                    return (

                                        <tr>
                                            <td>{source.name}</td>
                                            <td>{source.url}</td>
                                            <td>{source.category_id.name}</td>
                                            <td>
                                                <button type="button" className="btn btn-success" onClick={() => { this.selectSource(source); this.modalInsertar() }}><img src={iconEdit} /></button>
                                                <button type="button" className="btn btn-danger" onClick={() => { this.deleteSource(source._id) }}><img src={iconDelete} /></button>
                                            </td>
                                        </tr>

                                    )
                                })}



                            </tbody>
                        </table>
                        <div>
                            <button className="btn btn-secondary" onClick={() => { this.setState({ tipoModal: 'insertar' }); this.modalInsertar() }}>Add new</button>
                        </div>
                    </main>
                </div>

                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader style={{ display: 'block' }}>
                    </ModalHeader>
                    <ModalBody>
                        <div className="form-group mb-3">
                            <input type="hidden" value={form ? form._id : ''} name="id" />
                            <input type="hidden" value={form ? form.user_id : ''} name="user_id" />
                            <input type="text" className="form-control" placeholder="Name" value={form ? form.name : ''}
                                aria-describedby="inputGroupPrepend" required name="name" onChange={this.handleChange} />

                        </div>
                        <div className="form-group mb-3">
                            <input type="text" className="form-control" placeholder="Url" value={form ? form.url : ''}
                                aria-describedby="inputGroupPrepend" required name="url" onChange={this.handleChange} />
                        </div>


                        <div className="form-group">
                            <Input type="select" name="category_id" onChange={this.handleChange}>
                                <option >Select a category</option>
                                {this.state.categories.map(category => {
                                    return (

                                        <option selected={this.state.form.category_id == category._id} value={category._id}  >{category.name}</option>

                                    )
                                })}


                            </Input>
                        </div>
                    </ModalBody>

                    <ModalFooter>
                        {this.state.tipoModal == 'insertar' ?
                            <button className="btn btn-success" onClick={() => this.postSources()}>
                                Insertar
                            </button> : <button className="btn btn-secondary" onClick={() => this.patchSource()}>
                                Actualizar
                            </button>
                        }
                        <button className="btn btn-danger" onClick={() => { this.modalInsertar(); this.cleanForm() }}>Cancelar</button>
                    </ModalFooter>
                </Modal>

                <Footer />
            </div>






        )
    }
}