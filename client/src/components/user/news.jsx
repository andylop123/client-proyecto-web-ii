import React from 'react';

//icons
import iconDelete from '../../assets/img/icons8_delete_bin_32px.png';
import iconEdit from '../../assets/img/icons8_edit_32px_1.png';
//axios
import axios from 'axios';
//navBar component
import { NavBar } from './index';
//footer component
import { Footer } from './index';


export class News extends React.Component {
    state = {
        url: 'http://localhost:3000/Category',
        categories: [],
        user: '',
        news: [],
        urlGetNews: 'http://localhost:3000/newslist',
        urlGetNewsCategory: 'http://localhost:3000/newsCategory',

    }

    /*
    Este solicita al servidor las categorias registradas en el servidor
      */
    categoryGet = async () => {
        await axios.get(this.state.url).then(response => {
            this.setState({ categories: response.data })

        }).catch(err => {
            console.log(err.message)
        })


    }

      /*
    Este metodo solicita las noticias guradadas en el servidor
    */
    newsGet = async () => {
        await axios.get(this.state.urlGetNews, {
            params: {
                id: this.state.user.user._id
            }
        }).then(response => {
            this.setState({ news: response.data })
            console.log(this.state.news);

        }).catch(err => {
            console.log(err.message)
        })
    }


      /*
    Este metodo da un formato a la fecha que traen las noticias del servidor
    */
    dateFormat = (date) => {
        const f = new Date(date);
        return f.getDate() + "/" + f.getMonth() + "/" + f.getFullYear();
    }

      /*
    Este metodo da formato a al texto de las noticias del servidor
    */
    trimData = (content) => {
        var respuesta = content;
        var longitud = 200;
        if (content.length > longitud) {
            respuesta = content.substr(0, longitud - 3) + "..."
        }
        return respuesta;
    }

      /*
    Este metodo selecciona las categorias guardadas en el servidor
    */
    selectCategory = async (category) => {
        if (category) {
            await axios.get(this.state.urlGetNewsCategory, {
                params: {
                    category_id: category._id,
                    user_id: this.state.user.user._id
                }
            }).then(response => {
                this.setState({ news: response.data })
                console.log(this.state.news);

            }).catch(err => {
                console.log(err.message)
            })

        } else {
            await axios.get(this.state.urlGetNews, {
                params: {
                    id: this.state.user.user._id
                }
            }).then(response => {
                this.setState({ news: response.data })
                console.log(this.state.news);

            }).catch(err => {
                console.log(err.message)
            })
        }
    }






    /*
    Este metodo ejecuta las funciones de obtener categoria y obtener noticias
    una vez el componente renderice
    */
    componentDidMount() {
        this.categoryGet();
        this.newsGet();

    }


      /*
    Este metodo recoge el usuario logueado
    */
    getUserLogin() {
        const userLogin = JSON.parse(localStorage.getItem('user'));
        this.state.user = ({ user: userLogin });
    }



    render() {
        const { user } = this.state;
        if (user.length == 0) {
            this.getUserLogin();
        }
        return (
            <div className="container">
                <NavBar />
                <div className="containter">
                    <div className="container mt-5 d-flex align-items-center flex-column bg-white text-secondary ">
                        <h4 className="display-6 text-center">Your Unique News Cover</h4>
                        <hr className="my-4 bg-secondary w-25" />
                    </div>

                </div>

                {/* categories */}
                <div className="container  mb-5 pb-5 d-flex flex-column align-items-center w-75">
                    <div className="card-group ">
                        <button type="button" className="btn btn-secondary  m-1" onClick={() => { this.selectCategory(null); }}>Portada</button>
                        {this.state.categories.map(category => {
                            return (
                                <button type="button" className="btn btn-secondary  m-1" onClick={() => { this.selectCategory(category); }} >{category.name}</button>
                            );
                        })}



                    </div>
                </div>

                <div className="container ">
                    <main className="bg-white  ">
                        <div className="row" id='datos'>
                            {this.state.news.map(news => {
                                return (
                                    <div className="col mb-4 col-md-6 col-lg-4" >
                                        <div className="card">
                                            <div className="card-body">
                                                <p className="card-text" key={news._id}>{this.dateFormat(news.pubDate)}</p>
                                                <h5 className="card-title">{news.title}</h5>
                                                <p className="card-text">{this.trimData(news.content)}</p>
                                                <a className="btn btn-secondary btn-sm " id="button-login"
                                                    href={news.link}>See
                                                    News</a>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}


                        </div>
                    </main>
                </div>


                <Footer />
            </div>
        )
    }

}