import React from 'react';

import noticias from '../../assets/img/noticias.svg';
import userIcon from '../../assets/img/icons8_user_32px_2.png';
import { Link, Redirect } from 'react-router-dom';

export class NavBar extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            logOut: false,
            user: ''
        }

        this.getUserLogin = this.getUserLogin.bind(this);
    }



    //this method make all the logic to logOut the user
    logOut() {
        localStorage.removeItem('user');
        this.state.logOut = ({ logOut: true })

    }

      /*
    Este extrae el usuario logueado en la maquina del cliente
    */
    getUserLogin() {
        const userLogin = JSON.parse(localStorage.getItem('user'));
        if (userLogin == null) {
            this.logOut();
        } else {
            this.state.user = ({ user: userLogin });
        }

    }

    render() {
        const { user } = this.state;
        if (user.length == 0) {
            this.getUserLogin();
        }

        return (

            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center">
                    <div className="container-fluid">
                        <img src={noticias} width="230" height="80"
                            className="d-inline-block align-top" alt="" loading="lazy" />
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                            <ul className="navbar-nav text-secondary">
                                <li className="nav-item dropdown bg-secondary">
                                    <a className="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">
                                        <img src={userIcon} width="20"
                                            height="20" className="d-inline-block align-top" alt="" loading="lazy" /> {this.state.user.user.firstName}</a>
                                    <ul className="dropdown-menu">
                                        <li> <a className="nav-link text-secondary" href="/news" tabIndex="-1"
                                            aria-disabled="true">News</a></li>

                                        <li> <a className="nav-link text-secondary" href="/newsSources" tabIndex="-1"
                                            aria-disabled="true">NewsSources</a></li>


                                        <li> <a onClick={this.logOut} className="nav-link bg-secondary text-white"
                                            href="/ " tabIndex="-1"
                                            aria-disabled="true"><img
                                                src={userIcon}
                                                width="20" height="20" height="20" className="d-inline-block align-top" alt=""
                                                loading="lazy" /> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

        )
    }
}