import React from 'react'

export class Footer extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="container p-3 footer">
                <footer className="bg-white text-muted py-5">
                    <ul className="nav justify-content-center ">
                        <li className="nav-item active ">
                            <a className="nav-link text-secondary " href="">My Cover </a>
                        </li>
                        <li className="nav-item ">
                            <a className="nav-link text-secondary ">|</a>
                        </li>
                        <li className="nav-item ">
                            <a className="nav-link text-secondary " href="">About</a>
                        </li>
                        <li className="nav-item ">
                            <a className="nav-link text-secondary ">|</a>
                        </li>
                        <li className="nav-item ">
                            <a className="nav-link text-secondary " href="# ">Help</a>
                        </li>
                    </ul>
                    <ul className="nav justify-content-center ">
                        <a className="nav-link text-secondary " href="# " aria-disabled="true ">© My News Cover
                        </a>
                    </ul>

                </footer>
            </div>
        )
    }
}