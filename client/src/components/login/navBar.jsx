import React from 'react';

import noticias from '../../assets/img/noticias.svg';
import userIcon from '../../assets/img/icons8_user_32px_2.png';
import { Link, Redirect } from 'react-router-dom';

export class NavBar extends React.Component {


    constructor(props) {
        super(props)

    }

    render() {


        return (

            <div className="container">
                <nav className="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center">
                    <div className="container-fluid">
                        <img src={noticias} width="230" height="80"
                            className="d-inline-block align-top" alt="" loading="lazy" />
                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                            <ul className="navbar-nav text-secondary">
                                <li className="nav-item bg-secondary">
                                <Link to="/" className="nav-link  text-white" role="button">
                                        <img src={userIcon} width="20"
                                            height="20" className="d-inline-block align-top" alt="" loading="lazy" /> Login</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

        )
    }
}