
import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
//navBar component
import { NavBar } from './index';
//footer component
import { Footer } from './index';
import axios from 'axios'

export class Register extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        }
        this.changeFirstName = this.changeFirstName.bind(this);
        this.changeLastName = this.changeLastName.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

      /*
    Este metodo recoge el valor del campo de texto first name
    */
    changeFirstName(event) {
        this.setState({
            firstName: event.target.value
        })
    }

      /*
    Este metodo recoge el valor del campo de texto last name
    */
    changeLastName(event) {
        this.setState({
            lastName: event.target.value
        })
    }

      /*
    Este metodo recoge el valor del campo de texto email
    */
    changeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }

      /*
    Este metodo recoge el valor del campo de texto password
    */
    changePassword(event) {
        this.setState({
            password: event.target.value
        })
    }

      /*
    Este metodo recoge los valores guardados en estado y los envia al servidor para realizar un registro de un usuario
    */
    onSubmit(event) {
        event.preventDefault();
        const user = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            password: this.state.password
        }


        axios.post('http://localhost:3000/user', user)
            .then(response => console.log(response.data))
            .catch(err => console.log(err))

        this.setState({
            firstName: '',
            lastName: '',
            email: '',
            password: ''
        })

        this.setState({ redirect: true });
    }



    render() {
        const { redirect } = this.state;
        if (redirect) {
            return <Redirect to='/' />
        }

        return (
            <div>
                <NavBar />

                <div className="container mt-5 jumbotron d-flex align-items-center flex-column  bg-white ">
                    <h4 className="display-6 text-secondary">User Registration</h4>
                    <hr className="my-4 bg-secondary w-25 " />
                </div>


                <div className="container">
                    <main className="bg-white mt-5 pt-0" >
                        <form onSubmit={this.onSubmit} className="text-center">
                            <div className="form-row d-flex flex-column align-items-center pt-5">
                                <div className="form-group col-md-3 mb-3">
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="First Name"
                                        onChange={this.changeFirstName} />
                                </div>
                                <div className="form-group col-md-3 mb-3" >
                                    <input type="text" class="form-control" aria-describedby="emailHelp" placeholder="Last Name"
                                        onChange={this.changeLastName} />
                                </div>
                                <div className="form-group col-md-3 mb-3">
                                    <input type="email" class="form-control" aria-describedby="emailHelp" placeholder="Email"
                                        onChange={this.changeEmail} />
                                </div>
                                <div className="form-group col-md-3 mb-3">
                                    <input type="password" class="form-control" aria-describedby="emailHelp" placeholder="Password"
                                        onChange={this.changePassword} />
                                </div>
                                <div className="form-group ">
                                    <button type="submit" class="btn btn-primary mr-2">Register</button>
                                    <Link to="/" class="btn btn-secondary m-2" role="button">Cancel</Link>
                                </div>
                            </div>
                        </form>
                    </main>
                </div>


                <Footer />
            </div>

        )
    }
}