import React from "react";

import Session from 'react-session-api'
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios'
//img
import noticias from '../../assets/img/noticias.svg';
import userIcon from '../../assets/img/icons8_user_32px_2.png';

export class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: '',
            urlNS: "http://localhost:3000/newsSourceCount",
            cantNS: 0,
            type: '',
            email: '',
            password: ''
        }
        this.changeEmail = this.changeEmail.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.redirect = this.redirect.bind(this);
    }







    /*
    Este metodo recoge el valor del campo de texto email
    */
    changeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }

    /*
  Este metodo recoge el valor del campo de texto password
  */
    changePassword(event) {
        this.setState({
            password: event.target.value
        })
    }

    /*
      Este metodo recoge el los valores en el estado del componente y los envia 
  al servidor para comprobar si existe un usuario con esos datos
  */
    onSubmit(event) {
        event.preventDefault();
        const user = {
            params: {
                email: this.state.email,
                password: this.state.password
            }
        }

        axios.get('http://localhost:3000/user', user)
            .then(response => {
                this.redirect(response);
            });

    }




    /*
      Este metodo extraer las fuentes de noticias asociadas a un usuario
      */
    async newsSourceGet(user) {
        await axios.get(this.state.urlNS, {
            params: {
                user_id: user._id
            }
        }).then(response => {
            this.setState({
                cantNS: response.data,
                type: user.rol_id.name

            })

            console.log(response.data)
        }).catch(err => {
            console.log(err.message)
        })


    }

    /*
    Este metodo es el encargado de guardar el usuario logueado en la maquina del cliente
     */
    redirect(user) {
        if (user.data) {
            this.setState({ user: user.data });
            this.newsSourceGet(this.state.user)
            localStorage.setItem('user', JSON.stringify(this.state.user));

        }

    }


    render() {
        const { cantNS, type } = this.state;

        console.log(type, cantNS);

        if (type === 'User') {
            if (cantNS === 0) {
                return <Redirect to='/newsSources' />
            } else {
                return <Redirect to='/news' />
            }
        } else if (type === 'Admin') {
            return <Redirect to='/Category' />
        } else {

        }
        return (
            <div>
                <div className="container">
                    <nav className="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center">
                        <div className="container-fluid">
                            <img src={noticias} width="230" height="80"
                                className="d-inline-block align-top" alt="" loading="lazy" />
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                                <ul className="navbar-nav text-secondary">
                                    <li className="nav-item bg-secondary">
                                        <a className="nav-link  text-white" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">
                                            <img src={userIcon} width="20"
                                                height="20" className="d-inline-block align-top" alt="" loading="lazy" /> Login</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>

                <div className="container mt-5 jumbotron d-flex align-items-center flex-column  bg-white ">
                    <h4 className="display-6 text-secondary">User Login</h4>
                    <hr className="my-4 bg-secondary w-25 " />
                </div>


                <div className="container">
                    <main className="bg-white mt-5 pt-0" >
                        <form onSubmit={this.onSubmit} className="text-center">
                            <div className="form-row d-flex flex-column align-items-center pt-5">

                                <div className="form-group col-md-3 ">
                                    <input type="email" className="form-control " id="exampleInputEmail1"
                                        aria-describedby="emailHelp" placeholder="Email Address" name="email" onChange={this.changeEmail} />
                                </div>
                                <br />
                                <div className="form-group col-md-3">
                                    <input type="password" className="form-control" id="exampleInputPassword1"
                                        placeholder="Password" name="password" onChange={this.changePassword} />
                                </div>


                                <br />
                                <hr className=" bg-secondary w-50 " />
                                <label className="form-check-label text-secondary mb-3 ">If you don't have an account <Link to="/Register" role="button">Singup Here</Link> </label>
                                <div className="form-group ">
                                    <button type="submit " className="btn btn-secondary btn-sm m-1 mb-4"
                                        id="button-login">Login</button>
                                </div>
                            </div>
                        </form>
                    </main>
                </div>


                {/* footter */}
                <div className="container p-3 footer">
                    <footer className="bg-white text-muted py-5">
                        <ul className="nav justify-content-center ">
                            <li className="nav-item active ">
                                <a className="nav-link text-secondary " href="">My Cover </a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary ">|</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary " href="">About</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary ">|</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary " href="# ">Help</a>
                            </li>
                        </ul>
                        <ul className="nav justify-content-center ">
                            <a className="nav-link text-secondary " href="# " aria-disabled="true ">© My News Cover
                            </a>
                        </ul>
                    </footer>
                </div>
            </div>

        )
    }



}






