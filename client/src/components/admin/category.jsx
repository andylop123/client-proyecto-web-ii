import React from 'react';


//img
import noticias from '../../assets/img/noticias.svg';
import userIcon from '../../assets/img/icons8_user_32px_2.png';
import iconDelete from '../../assets/img/icons8_delete_bin_32px.png';
import iconEdit from '../../assets/img/icons8_edit_32px_1.png';
import axios from 'axios';
//modal
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import { Redirect } from 'react-router-dom';
const url = 'http://localhost:3000/Category';
const urlNSByCategory = 'http://localhost:3000/newsSourceCategory'
const urlNewsByCategory = 'http://localhost:3000/newsDeleteCategory'

export class Category extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            logOut: false,
            user: '',
            categories: [],
            modalInsertar: false,
            form: {
                id: '',
                name: '',
                user_id: '',
                tipoModal: '',
            }
            
        }
        this.logOut = this.logOut.bind(this);
    }






    /*
    Este metodo cambia el estado del modal, en este caso el modal se maneja
    por un variable booleana
    */
    modalInsertar = () => {
        this.setState({ modalInsertar: !this.state.modalInsertar });
    }




    
    /*
    Este metodo hace una llamado al servidor, recibe las categorias existentes en el servidor
    */
    categoryGet = async () => {
        await axios.get(url).then(response => {
            this.setState({ categories: response.data })

        }).catch(err => {
            console.log(err.message)
        })


    }

    
    /*
    Este metodo inserta una categoria en el servidor por medio un post,
    no recibe nada a cambio
    */
    categoryPost = async () => {
        await axios.post(url, this.state.form).then(response => {
            this.modalInsertar();
            this.categoryGet();
        }).catch(err => {
            console.log(err.message)
        })

    }

    
    /*
    Este metodo permite editar una categoria en el servidor, por medio de patch 
    */
    categoryPatch = async () => {
        await axios.patch(url + '?id=' + this.state.form.id, this.state.form).then(response => {
            this.modalInsertar();
            this.categoryGet();
        }).catch(err => {
            console.log(err.message)
        })

    }

    
    /*
    Este metodo elimina una categoria existente en el servidor
    */
    categoryDelete = async (id) => {

        await axios.delete(urlNewsByCategory,{params:{
            "id":id
        }}).then(response => {
            this.categoryGet();
        }).catch(err => {
            console.log(err.message)
        })
        
        await axios.delete(urlNSByCategory,{params:{
            "id":id
        }}).then(response => {
            this.categoryGet();
        }).catch(err => {
            console.log(err.message)
        })


        await axios.delete(url,{params:{
            "id":id
        }}).then(response => {
            this.categoryGet();
        }).catch(err => {
            console.log(err.message)
        })

    }



    
    /*
    Este metodo selecciona los datos de una categoria y los devuelve
    */
    selectCategory = (category) => {
        this.setState({
            tipoModal: 'actualizar',
            form: {
                id: category._id,
                name: category.name,
                user_id: category.user_id
            }
        });

        console.log(this.state.form)
    }
    
    
    /*
    Este metodo de react nos permite llamar metodos una vez el componente renderice
    */
    componentDidMount() {
        this.categoryGet();
    }

    
    
    /*
    Este metodo utilizado en react nos permite extraer valoes de una campo de texto
    */
    handleChange = async e => {
        e.persist();
        await this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value,
                [e.target.user_id]: this.state.user.user._id,

            }
        });
        console.log(this.state.form)
    }

    
    
    /*
    Este metodo es el encargado de realizar el log out de un usuario
    */
    logOut(){
        localStorage.removeItem('user');
        this.state.logOut = ({logOut: true})
         
    }

    
    /*
    Este metodo nos revela el usuario que realizao inicio de session
    */
    getUserLogin() {
        const userLogin = JSON.parse(localStorage.getItem('user'));
        if(userLogin == null){
            this.logOut();
        }else{
            this.state.user = ({ user: userLogin });
        }
      
    }

   



    render() {
        const { user } = this.state;
        if (user.length == 0) {
            this.getUserLogin();    
        }

        if(this.state.logOut){
            console.log('entra')
            return  <Redirect to='/' />
        }

        const { form } = this.state;
        return (
            
            <div>


                <div className="container">
                    <nav className="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center">
                        <div className="container-fluid">
                            <img src={noticias} width="230" height="80"
                                className="d-inline-block align-top" alt="" loading="lazy" />
                            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                                <ul className="navbar-nav text-secondary">
                                    <li className="nav-item dropdown bg-secondary">
                                        <a className="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">
                                            <img src={userIcon} width="20"
                                                height="20" class="d-inline-block align-top" alt="" loading="lazy" /> {this.state.user.user.firstName}</a>
                                        <ul className="dropdown-menu">
                                            <li> <a onClick = {this.logOut} className="nav-link bg-secondary text-white"
                                                href="/" tabindex="-1"
                                                aria-disabled="true"><img
                                                    src={userIcon}
                                                    width="20" height="20" height="20" className="d-inline-block align-top" alt=""
                                                    loading="lazy" /> Logout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>


                <div className="container mt-5 d-flex align-items-center flex-column bg-white text-secondary ">
                    <h4 className="display-6 text-center">Categories</h4>
                    <hr className="my-4 bg-secondary w-25" />
                </div>



                <div className="container ">
                    <main className="bg-white  d-flex flex-column align-items-center pt-4 pt-0 "  >
                        <table className="table  table-dark table-striped  w-50">
                            <thead>
                                <tr>
                                    <th scope="col">Category</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                                {this.state.categories.map(category => {
                                    return (
                                        <tr>
                                            <td>{category.name}</td>
                                            <td>
                                                <button type="button" className="btn btn-success" onClick={() => { this.selectCategory(category); this.modalInsertar() }}><img src={iconEdit} /></button>
                                                <button type="button" className="btn btn-danger" onClick={() => { this.categoryDelete(category._id) }}><img src={iconDelete} /></button>
                                            </td>
                                        </tr>
                                    );
                                })}

                            </tbody>
                        </table>
                        <div>
                            <button class="btn btn-secondary" onClick={() => { this.setState({ form: null, tipoModal: 'insertar' }); this.modalInsertar() }}>Add new</button>
                        </div>
                    </main>
                </div>

                <div className="container p-5 footer">
                    <footer className="bg-white text-muted py-5">
                        <ul className="nav justify-content-center ">
                            <li className="nav-item active ">
                                <a className="nav-link text-secondary " href="">My Cover </a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary ">|</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary " href="">About</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary ">|</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link text-secondary " href="# ">Help</a>
                            </li>
                        </ul>
                        <ul className="nav justify-content-center ">
                            <a className="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
                            </a>
                        </ul>
                    </footer>
                </div>

                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader style={{ display: 'block' }}>
                    </ModalHeader>
                    <ModalBody>
                        <div className="form-group">
                            <input type="hidden" value={form ? form._id : ''} name="id" />
                            <input type="hidden" value={form ? form.user_id : ''} name="user_id" />
                            <input type="text" class="form-control" placeholder="Category" value={form ? form.name : ''}
                                aria-describedby="inputGroupPrepend" required name="name" onChange={this.handleChange} />
                        </div>
                    </ModalBody>

                    <ModalFooter>
                        {this.state.tipoModal == 'insertar' ?
                            <button className="btn btn-success" onClick={() => this.categoryPost()}>
                                Insertar
                            </button> : <button className="btn btn-secondary" onClick={() => this.categoryPatch()}>
                                Actualizar
                            </button>
                        }
                        <button className="btn btn-danger" onClick={() => this.modalInsertar()}>Cancelar</button>
                    </ModalFooter>
                </Modal>




            </div>



        )
    }
}